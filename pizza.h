#ifndef PIZZA_H
#define PIZZA_H

#include <stdio.h>

typedef struct pizza {
	int code;
	char name[50];
	char description[20];
	float price;
} Pizza;

// Prints the pizza
void printPizza(Pizza *p);

// Creates the pizza struct
Pizza *pizza(int code, char *name, char *categoria, float price);

// Saves pizza in the "out" file, in the current position of the cursor
void savePizza(Pizza *p, FILE *out);

// Reads a pizza in the "in" file, in the current position of the cursor
// Returns a pointer to the just read pizza from the file
Pizza *readPizza(FILE *in);

// Compares two pizzas
// 1 if all attributes are equal
// else returns 0
int comparePizza(Pizza *p1, Pizza *p2);

// Returns size of the struct pizza
int sizeOfPizza();

#endif //PIZZA_H