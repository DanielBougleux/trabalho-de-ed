#ifndef PTREE_H
#define PTREE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pizza.h"

//Structure of B+ Tree of pizzas
typedef struct arvpiz{ 
    /* activKeys is the number of active keys
    * isLeaf is a boolean that says if the node is leaf
    * lastIndex if a boolean that says if it's children are data
    * keys is an array of pizza codes */
    int activKeys, isLeaf, lastIndex, *keys;

    // children is an array to seek the node's children
    // brother is a pointer to seek the node's brother
    // pizzas is an array to seek the node's pizzas
    long *children, *pizzas, brother; 

}PTree;


//Structure of descriptor
typedef struct desc{
    //To organize interation between files of indexes and data
    //indexes and datas stores the names of the index file and data file respectively
    char *indexes, *datas, *pizzas;

    // boolean to say if the root in the data file or not
    int rootInData;

    // pointer seek to where the root is in the file
    long root;

    // Ramification factor
    int t;

}Desc;


//Allocates memory for a PTree node
PTree * create(int t);

//Allocates memory for a Desc node
Desc * createDescriptor(char * indexes, char * datas, int t);

//Make de inicial decisions for insertion
void inserts(Desc * descriptor, Pizza * pizza);

//Saves a node in the current posicion of the archives
void save_node(FILE * f, PTree * tree, int t);

//Try to read a node from de current posicion of the arquive. Returns NULL if it does not succeed;
PTree * read_node(FILE * f);

//Helper to open file and check success
FILE * openFile(const char * file, const char * type);

//Reads a PTree node from the current position of the cursor
PTree * readPTree(FILE * f, int t);

//Inserts in a not full node
void insertNotFull(PTree * pt, Desc * desc, Pizza * p);

#endif //PTREE_H