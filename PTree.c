#include "PTree.h"

Desc * createDescriptor(char * indexes, char * datas, int t){

    //Allocates memory for a Desc node
    Desc * novo = (Desc *)malloc(sizeof(Desc));
    //Guarantees that the memory block is filled with zero
    if(novo) memset(novo, 0, sizeof(Desc));

    //Copies the parameters
    strcpy(novo->datas, datas);
    strcpy(novo->indexes, indexes);
    novo->t = t;

    //Returns the node
    return novo;
}

PTree * create(int t){

    //Allocates memory for the node
    PTree * novo = (PTree*)malloc(sizeof(PTree));

    //Guarantees that the memory block is filled with zero
    if(novo) memset(novo, 0, sizeof(PTree));

    //Allocates memory for the array of keys
    novo->keys = (int*)malloc(sizeof(int)*((2*t)-1));

    //Allocates memory for the array of pointers to children
    novo->children = (long*)malloc(sizeof(long)*(2*t));

    //Sets every chindren to -1 by default
    int i;
    for(i = 0; i<(2*t); i++) novo->children[i] = -1;

    //Allocates memory for the array of seeks to pizza
    novo->pizzas = (long*)malloc(sizeof(long)*((2*t)-1));

    //The new made node is set to be leaf
    novo->isLeaf = 1;

    //Returns the node
    return novo;
}

void save_node(FILE * f, PTree * tree, int t){

    if(fwrite(tree->activKeys,sizeof(int), 1, f) != 1) exit(1);

    if(fwrite(tree->isLeaf,sizeof(int), 1, f) != 1) exit(1);

    if(fwrite(tree->lastIndex,sizeof(int), 1, f) != 1) exit(1);

    int i;
    for(i = 0; i<tree->activKeys;i++){
        if(fwrite(tree->keys[i],sizeof(int), 1, f) != 1) exit(1);
    }

    for(i = 0;  i<(2*t); i++){
        if(fwrite(tree->children,sizeof(long), 1, f) != 1) exit(1);
    }

    for(i = 0; i<tree->activKeys; i++){
        if(fwrite(tree->pizzas,sizeof(long), 1, f) != 1) exit(1);
    }

    if(fwrite(tree->brother,sizeof(long), 1, f) != 1) exit(1);

}
void insertNotFull(PTree * pt, Desc * desc, Pizza * p) {
    //Assigns i to last index of keys
    int i = pt->activKeys-1;

    //If the node we're trying to insert is a leaf
    if(pt->isLeaf) {
        
        //Searches the index of the last element smaller than p->code
        while((i>=0) && (p->code < pt->keys[i])){

            //Pushes foward the elements
            pt->keys[i+1] = pt->keys[i];
            i--;
        }

        //Assigns the code to the right position
        pt->keys[i+1] = p->code;

        //The new key that we just inserted
        pt->activKeys++;

        //Opens pizza file
        FILE * pizzaFile = openFile(desc->pizzas, "rb");

        //Sets the cursor to the last pizza inserted in the pizzas file
        fseek(pizzaFile, (-1)*sizeof(Pizza), SEEK_END);

        //Assigns the adress of the just inserted pizza
        long pizzaAdress = ftell(pizzaFile);

        //Assigns the adress to the pizzas array
        pt->pizzas[i+1] = pizzaAdress;

        //Closes the pizza file
        fclose(pizzaFile);

        //Opens the data file
        FILE * dataFile = openFile(desc->datas, "wb");

        //Writes the updated PTree node in the data file
        fwrite(&pt, sizeof(PTree), 1, dataFile);

        //Closes the data file
        fclose(dataFile);

        return;
    }

    //Assigns t to the ramification factor
    int t = desc->t;

    //If the node isn't a leaf
    //Searches the index of the last key smaller than p->code
    while((i>=0) && (p->code < pt->keys[i])) i--;

    /*Assigns i to the first child with bigger keys than
    * the last key with smaller keys than p->code*/
    i++;

    //Initializes the child to operate;
    PTree * child;

    //If the child of PTree node is in the data file
    if(pt->lastIndex) {
        FILE * dataFile = openFile(desc->datas, "rb");
        fseek(dataFile, pt->children[i], SEEK_SET);
        child = readPTree(dataFile, t);
    }

    //If the child of PTree node is in the index file
    else {
        FILE * indexFile = openFile(desc->indexes, "rb");
        fseek(indexFile, pt->children[i], SEEK_SET);
        child = readPTree(indexFile, t);
    }

    //If the child where p->code will be inserted is full
    if(child->activKeys == ((2*t)-1)){

        //splits the child node updating the current node
        //pt = splits(x, (i+1), x->children[i]);

        //If the key that came up is smaller than p->code
        if(p->code > pt->keys[i]) i++;
    }

    //Inserts in the child "i", or it's grandchild
    insertNotFull(child, desc, p);

    return;
}

PTree * readPTree(FILE * f, int t) {
    //Creates a node to hold the information read from the file
    PTree * novo = create(t);

    //Reads all the information in the file related to the PTree node
    fread(&novo->activKeys, sizeof(int), 1, f);
    fread(&novo->isLeaf, sizeof(int), 1, f);
    fread(&novo->lastIndex, sizeof(int), 1, f);
    fread(novo->keys, sizeof(int), 2*t-1, f);
    fread(novo->children, sizeof(long), (2*t), f);
    fread(novo->pizzas, sizeof(long), 2*t-1, f);
    fread(novo->brother, sizeof(long), 1, f);

    //Returns the node with all the informations
    return novo;
}

FILE * openFile(const char * file, const char * type) {
    FILE * f = openf(file, type);
    if(!f) exit (1);
}
